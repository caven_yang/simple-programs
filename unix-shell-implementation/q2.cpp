#include <unistd.h>     // getpid(), getcwd()
#include <sys/types.h>  // type definitions, e.g., pid_t
#include <sys/wait.h>   // wait()
#include <signal.h>     // signal name constants and kill()
#include <iostream>
#include <vector>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <fcntl.h>
//#include "aux.h"

using namespace std;




int main(){
  char command[128];
  int status = 0;
  vector<char > row;
  vector<vector<char > > hist;
  cout << "welcome to my shell\tpiggly wiggly\n";
  while(1){
    char buf[128];
    if( getcwd(buf,128) == NULL ){
      perror("getcwd error\n");
    }
    
    else{
      cout << buf << "< ";
  }
    switch (status){
      case 0:{
	cout << "My Shell > ";
	cin.getline(command,128);
	
	
      }
      case 1:{
	status = 0;}
    }
    
    
    if ( strcmp(command,"!#") != 0){
	for(int i=0;command[i] != '\0';i++){
	  row.push_back(command[i]);}
	hist.push_back(row);
	row.clear();
    }
      
      
 
    
    char* com1 = strtok( command," ");
    
    char* temp = com1;
    vector<char *> arg;
    
    while(temp != NULL){
      
      arg.push_back(temp);
      //cout << temp << endl;
      temp = strtok(NULL," ");

    }
 
    int size = hist[hist.size()-1].size();
    char cmd[size];
    for(int j=0; j<size; j++){
      cmd[j] = hist[hist.size()-1][j];}
      cmd[size] = '\0';
      while(1){
	if(sizeof cmd == 0){
	  break;
	}
      

    
    
    /*here:
     * cmd stores the command you have just put in;
     * command is now only the first word
     * com1 is the same as command
     * arg is a vector of char[] storing the tokens of the command you put in
     */
    
    
    
    
    
    
  /*dealing with exit
   *
   * 
   * 
   * 
   * 
    */  
  if( !strcmp( com1,"exit") ){
    cerr << "are you sure you want to exit?\t press y to exit\n";
    char y_n[128];
    cin.getline(y_n,128);
    if(y_n[0] == 'y'){
      return 0;
    }
    
    else{
    }
  }
  
  else if( (!strncmp(com1, "./",2) )	){
    //cout << arg[1] << endl << arg[2] << endl << arg[3] << endl << arg[4] << endl;
    bool ok = true;
    int size_io = arg.size()-1;
    
    //cout << "input parameters: \t" << size_io << endl;
    if(size_io == 0){
      cout << "you can redirect stdin and stdout\n";
      system(com1);}
      
    else{
      
      if((size_io != 4) && (size_io != 2)){
	ok = false;
	cerr << "put in 2 or 4 arguments\n";
      }
      if(size_io == 4){
	if( !strcmp(arg[1],arg[3]) ){
	  ok = false;
	  cerr << "one input and one output\n";
	}
      }
 
      if( (strcmp(arg[1],"<")) &&(strcmp(arg[1],">"))){
	cerr << "No.2 term is not < or >\n";
	ok = false;
      }
      
      if(size_io == 4){
	if( (strcmp(arg[3],"<")) &&(strcmp(arg[3],">"))){
	  ok = false;
	  cerr << "No.4 term is not < or >\n";
	}
      }
      //cout << "size is " << size << endl;
      char in[size];
      char out[size];
      bool defin = false;
      bool defout = false;
      //cout << "getting in and out\n";
      
	if(!strcmp(arg[1],"<")){
	  strcpy(in,arg[2]);
	  //cout << " copy in from arg2\n";
	  defin = true;
	  
	}
	if(!strcmp(arg[1],">")){
	  strcpy(out,arg[2]);
	  //cout << " copy out from arg2\n";
	  defout = true;
	}
	
	
      
      
      if(size_io == 4){
	if(!strcmp(arg[3],"<")){
	  strcpy(in,arg[4]);
	  defin = true;
	  
	}
	if(!strcmp(arg[3],">")){
	  strcpy(out,arg[4]);
	  defout = true;
	}
      }
      if(defout == true){
	cout << "write to :\t" << out << endl;
	FILE * os = freopen(out,"a+",stdout);
      }
      if(defin == true){
	cout << "read from :\t" << in << endl;
	FILE * is = freopen(in,"r",stdin);
      }
      if (ok == true){

	char** argv = new char*[arg.size()+1];

      for ( int k = 0; k < arg.size(); k++ ){
	argv[k] = arg[k];
	cerr << argv[k] << "\n";
      }
      //argv is the array version of vector args ending with NULL(2D array)
	argv[arg.size()] = NULL;
	pid_t pid;
	int sstatus;
	if ((pid = fork()) < 0) {     /* fork a child process           */
          printf("*** ERROR: forking child process failed\n");
          exit(1);
	}
	else if (pid == 0) {          /* for the child process:         */
          if (execvp(com1, argv) < 0) {     /* execute the command  */
               printf("*** ERROR: exec failed\n");
               exit(1);
          }
     }
     else {                                  /* for the parent:      */
          while (wait(&sstatus) != pid);       /* wait for completion  */
               
     }
	  
	  //execlp(com1,NULL);
	  
	  if(defin == true){
	    //fclose(stdin);
	    
	    freopen("/dev/stdin","r",stdin);
	    cerr << "stdin restored!\n";
	    
	  }
	  if(defout == true){
	    //fclose(stdout);
	    
	    freopen("/dev/stdout","w",stdout);
	    cerr << "stdout restored!\n";
	  }
	  
	
      }
    
      else{
	cerr << "the format is wrong; use:\n"
	    << "./a.out < input > output\n"
	    << "./a.out < input\n"
	    << "./a.out > output\n";
      }
	      
      
 
    

    
   
    //fclose(stdout);
  }
  perror(cmd);
  }
  
  /*dealing with command that isn't found by the above situations
   * such commands will be sent to the shell and do it the normal way
   * 
   * 
   * 
   * 
   * 
   */
  else {
    
    cout << ("normal shell command\n");
    system(cmd);
    
  }
  
    
    
    break;
  }
  //cout << " byebye\n";
  //return 0;
  
}
}
    
    
    
      
    
