#ifndef AUX_H
#define AUX_H

#include <unistd.h>     // getpid(), getcwd()
#include <sys/types.h>  // type definitions, e.g., pid_t
#include <sys/wait.h>   // wait()
#include <signal.h>     // signal name constants and kill()
#include <iostream>
#include <vector>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <fcntl.h>

#define size 128

using namespace std;


void welcome();

//int strcpy_by_value(const char source[],char destination[],int len);

void printVectorChar(vector< char * > & eg, size_t n);


#endif