#include <unistd.h>     // getpid(), getcwd()
#include <sys/types.h>  // type definitions, e.g., pid_t
#include <sys/wait.h>   // wait()
#include <signal.h>     // signal name constants and kill()
#include <iostream>
#include <vector>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
using namespace std;


int main()
{
  while ( true )
    {
      // Show prompt.
      
      //cout << get_current_dir_name () << "$ " ;
      cout << "My B Shell > ";
      char command[128];
      cin.getline( command, 128 );
      
      vector<char*> args;
      char* prog = strtok( command, " " );
      cout << "\tprog is " << prog <<"\n";//prog stores the first command-word
      char* tmp = prog;

      while ( tmp != NULL )
	{
	  args.push_back( tmp );
	  
	  tmp = strtok( NULL, " " );
	}//args is an array storing word by word the command line
      //1. below is what I used to exam what consists of vector args
      //cout << "\targs is now: " ;
      for( int i=0; i < args.size(); i++){
	cout << args[i] << " ";
	
      }
      cout << "\n";
      //1. ends
      char** argv = new char*[args.size()+1];
      //cout << "\targv is :\n";
      for ( int k = 0; k < args.size(); k++ ){
	argv[k] = args[k];
	cout << argv[k] << "\n";
      }
// //       //argv is the array version of vector args ending with NULL(2D array)
      argv[args.size()] = NULL;
      

      
      
      if ( strcmp( command, "exit" ) == 0 )//first c-word is exit
	{
	  cerr << "exit!\n";
	  return 0;
	  
	}
	
      
      else
	{  
	  //if(false)
	  if (!strcmp (prog, "cd"))//first c-word is cd
	    { 
	      //system(command);
	      if (argv[1] == NULL)
		{
		  chdir ("/");
		}
	      else
		{
		  chdir (argv[1]);
		}
	      perror (command);
	    }
	  else
	    {
	       
/*
	      if(i == 1){
		freopen("input","r",stdin);
	      }
	      
	      else if( i==2){
	      freopen("output","a+",stdout);
	      }
	      
	      else if(i == 0){
		freopen("input","r",stdin);
		freopen("output","a+",stdout);
	      }
	      //system(command);*/
	      freopen("input","r",stdin);
	      pid_t kidpid = fork();
	      int status;
	      //cout << "\tkidpid is " << kidpid <<endl;
	      if (kidpid < 0)
		{
		  perror( "Internal error: cannot fork." );
		  return -1;
		}
	      else if (kidpid == 0)
		{
		  // I am the child.
		  if(execvp (prog, argv)<0){
		    cerr << "exec error!\n";
		    return 0;
		  }
		  
		  // The following lines should not happen (normally).
		  //perror( command );
		  return -1;
		}
	      else
		{
		  // I am the parent.  Wait for the child.
		  while ( wait( &status ) != kidpid )
		    {
		      //perror( "Internal error: cannot wait for child." );
		      //return -1;
		    }
		}
		cerr << "done!\n";
		/*
	      if(i==1){
	      freopen("/dev/stdin","r",stdin);
	      fclose(stdin);
	      }
	      
	      else if(i==2){
	      freopen("/dev/stdout","w",stdout);
	      fclose(stdout);
	      }
	      
	      else if(i==0){
		freopen("/dev/stdin","r",stdin);
		freopen("/dev/stdout","w",stdout);
		fclose(stdin);
		fclose(stdout);
	      }
	      */
		
		
	    fclose(stdin);
	    }
	}
    }
  
  return 0;
}
