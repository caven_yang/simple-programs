#include <unistd.h>     // getpid(), getcwd()
#include <sys/types.h>  // type definitions, e.g., pid_t
#include <sys/wait.h>   // wait()
#include <signal.h>     // signal name constants and kill()
#include <iostream>
#include <vector>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <fcntl.h>
#include "aux.h"


using namespace std;

int main(){
  welcome();
  vector<char *> history;
  char command[size];
  char *start;//command begins with start token
  vector<char *> argv;//used to store command tokens
  
  
  while(true){
    cout << "My Shell -> ";
    cin.getline(command,size);
    
    char cmd[strlen(command)]; 
    //strcpy_by_value(command,cmd,strlen(command));
    strcpy(cmd,command);//make a copy
    /*cmd is the same as command;but changing the command
     * won't modify cmd! both end with '\0'
     */ 	
	
    cmd[size] = command[size] = '\0';//to ensure
    history.push_back(cmd);
    /*history starts from 0 to history.size()-1
     * 
     * 
     * 
     */
    
     start = strtok(command," ");
     char *temp = start;
     while(temp != NULL){
       argv.push_back(temp);
       temp = strtok(NULL," ");
     }
     
    
  }
  
  
  
}