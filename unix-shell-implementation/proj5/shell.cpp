#include <unistd.h>     // getpid(), getcwd()
#include <sys/types.h>  // type definitions, e.g., pid_t
#include <sys/wait.h>   // wait()
#include <signal.h>     // signal name constants and kill()
#include <iostream>
#include <vector>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <fcntl.h>
#include <fstream>


using namespace std;
void sig_ctr_z ( int signum ){

	cout << "ctr+z\n";

	}	


void sig_ctr_c ( int signum ){

	cout << "ctr+c\n";

	}




int main(){
  char command[128];
  int status = 0;
  vector<char > row;
  vector<vector<char > > hist;
  cout << "welcome to my shell\tpiggly wiggly\n"
	  << "use $help to get a document for this shell. "
	     << "also procedures and questions to be solved are listed."
		<<" Let's take a look\n";
  while(1){
	signal(SIGINT,sig_ctr_c);
	signal(SIGTSTP,sig_ctr_z);     


	
	char buf[128];
    if( getcwd(buf,128) == NULL ){
      perror("getcwd error\n");
    }
    
    else{
      cout << buf << "< ";
  }
    switch (status){
      case 0:{
	cout << "My Shell > ";
	cin.getline(command,128);
	
	
      }
      case 1:{
	status = 0;}
    }
    
    
    if ( strcmp(command,"!#") != 0){
	for(int i=0;command[i] != '\0';i++){
	  row.push_back(command[i]);}
	hist.push_back(row);
	row.clear();
    }
      
      
 
    
    char* com1 = strtok( command," ");
    
    char* temp = com1;
    vector<char *> arg;
    
    while(temp != NULL){
      
      arg.push_back(temp);
      //cout << temp << endl;
      temp = strtok(NULL," ");

    }
 
    int size = hist[hist.size()-1].size();
    char cmd[size];
    for(int j=0; j<size; j++){
      cmd[j] = hist[hist.size()-1][j];}
      cmd[size] = '\0';
      while(1){
	if(sizeof cmd == 0){
	  break;
	}
      

    
    
    /*here:
     * cmd stores the command you have just put in;
     * command is now only the first word
     * com1 is the same as command
     * arg is a vector of char[] storing the tokens of the command you put in
     */
    
    
    
    
    
    
  /*dealing with exit
   *
   * 
   * 
   * 
   * 
    */  
  if( !strcmp( com1,"exit") ){
/*    cerr << "are you sure you want to exit?\t press y to exit\n";
    char y_n[128];
    cin.getline(y_n,128);
    if(y_n[0] == 'y'){
	cerr << "byebye\n";
	return 0;
    }
    
    else{
    }*/
	cerr << "byebye\n";
	return 0;
  }
  
  
/*dealing with history
 *history -c clears the history
 * 
 * 
 */
  else if( !strcmp( com1,"history") ){
    //cout << "arg[1] is :" << arg[1] << endl;
    if( arg.size() == 1 ){
      cout << "history is :\n";
      
      for (int j=0; j < hist.size(); j++){
	cout << j << ".";
	for(int k=0; k < hist[j].size(); k++){
	  cout << hist[j][k];}
	  cout << endl;
      }
      
      cout << "use !# to redo the command\n";
    }
    
    else if( !strcmp( arg[1],"-c") ){
      
      hist.clear();
      cout << "history cleared!\n";
    }
    else{
      cerr << "no command\n";
    }
    
    perror(cmd);
  }
  
  /*dealing with !#
   * !# num repeats the No.num command in history
   * !# alone repeats the most recent command
   * 
   * 
   * 
   * 
   * 
   */
  else if( !strcmp( com1,"!#" ) ){
    int redo;
    status = 1;  
      redo = atoi( arg[1]);
      cout << "will redo No." << redo << endl;
    
    //redo = redo -2;
    for(int j=0; j < hist[redo].size();j++){
      command[j] = hist[redo][j];
      cout << command[j];
    }
    command[hist[redo].size()] = '\0';
    cout << endl << hist[redo].size() << endl;
    perror(cmd);
    cout << "repeat :\t" << command << endl;
   
  }
  /*dealing with cd
   * 
   * 
   * 
   * 
   * 
   * 
   * 
   */
  else if( !strcmp( com1,"cd" ) ){
    if( arg[1] == NULL){
      chdir("/");
    }
    
    else{
      chdir(arg[1]);
    }
    
    perror(cmd);
  }
    
  /*dealing with jobs
   *
   *
   *
   *
   */  
  else if( !strcmp( com1,"jobs" ) ){
    cout << "under construction\n";
  }
  
  /*dealing with pwd
   * 
   * 
   * 
   * 
   * 
   */
  else if( !strcmp( com1,"pwd" ) ){
    char buf[128];
    if( getcwd(buf,128) == NULL ){
      perror("getcwd error\n");
    }
    
    else{
      cout << "current working directory: \n";
      cout << buf << endl;
  }
  perror(cmd);
  }




  /*dealing with more
   * 
   * 
   * 
   * 
   * 
   */
  else if( !strcmp( com1,"more" ) ){
    int fd = open(arg[1],O_RDONLY);
    //cout << "open " << arg[1] << endl;
    const int size = 128;
    char buf[size];
    ssize_t nread;
    if( fd != -1){
      nread = read(fd,buf,size);
      int len = strlen(buf)>size?size:strlen(buf);
      
      for(int k=0;k<len;k++){
	cout << buf[k];}
	cout << endl;
      cout << len << " characters displayed\n";
      if( len == size){
	cout << "press y to get more\n";
	char ans[10];
	cin.getline(ans,10);
	if( ans[0] == 'y' ){
	int offset = 0;
	char rebuf[size];
	
	  while(1){
	    lseek(fd,offset,SEEK_SET);
	    
	    nread = read(fd,buf,size);
	    lseek(fd,offset+size,SEEK_SET);
	    read(fd,rebuf,size);
    
	    len = strlen(buf)>size?size:strlen(buf);
	    //cout << "length is " << len << endl;
	    if(strncmp(buf,rebuf,size-1)== 0){
	      //cout << "\nbuf and rebuf is the same\n";
	      break;}
	    //cout << endl << "buf: \n";
	    for(int k=0;k<len;k++){
	      cout << buf[k];}
	   offset = offset + size;
	    
	  }
	  
	  
	
	}
      }
      
      else{
      }
    }
    
    else{
      cerr << "cannot open the file\n";
    }
    
    close(fd);
    cout << endl;
  }
  
  
  
  
  /*dealing with help
   * 
   * 
   * 
   * 
   * 
   */
  
  else if(!strcmp(com1,"help") ){
    string line;
    ifstream file ("help");
   
    if(file.is_open() ){
      while(file.good() ){
	getline(file,line);
	cout << line << endl;
      }
      
      file.close();
    }
    
   
  }
  
  
  /*dealing with ls
   * 
   * 
   * 
   * 
   * 
   */
  else if( !strcmp( com1,"ls" ) ){
    
    DIR *dir;
    struct dirent *read;
    if( arg.size() == 1){
      char pwd[128];
      getcwd( pwd,128);
      cout << pwd << "\tcontains the following items: \n";
      dir = opendir( ".");
    }
    
    else{
      cout << arg[1] << "\tcontains the following items: \n";
      dir = opendir( arg[1]);
    }
    if(dir != NULL){
    int i=0;
      while(read = readdir(dir)){
	i++;
	if(i%10==0){
	  cout << endl;}
	cout << read->d_name << "\t";
      }
 
      cout << endl;
    }
    else{
      cerr << "cannot open the directory\n";
    }
      closedir(dir);
    }
      
	/*try to deal with an executable
   	* format:
	* exec ./exe [parameter] will just execute the executable ./exec
	* exec -r ./exe [parameter] < input > output
	* 	will redirect the stdin/stdout
   	* 
  	* 
	*/

	else if( !strcmp( com1,"exec") ){
		pid_t pid = fork();
		int pstatus;
		int fork;

		if(pid ==0){
			cerr << "child process starts:\n";
			//cout << arg[1] << endl;
			
			if(! strcmp(arg[1],"-r")){
			
			  cerr << "stdin/stdout will be redirected\n";
			  cerr << "format:./exe -r < inputfile > outputfile\n";
			  int inMark = 2;//arg[inMark] labels "<"
			  for(;inMark < arg.size()+1;inMark++){
			    if(!strcmp(arg[inMark],"<") ){
			      break;}
			  }
			  cerr << "the redirected stdin file is\t" << arg[inMark+1] << endl;
			  
			  int outMark = 2;//arg[inMark] labels "<"
			  for(;outMark < arg.size()+1;outMark++){
			    if(!strcmp(arg[outMark],">") ){
			      break;}
			  }
			  cerr << "the redirected stdout file is\t" << arg[outMark+1] << endl;
			  
			  
			}
			
			else{
			  char **args = new char*[16];
		
			  //cerr << "arg.size() is:\t" << arg.size() << endl;
			  for(int i=1;i < arg.size()+1;i++){
			    args[i-1] = arg[i];//?? why is this legitimate?
			    //arg[i] is pointer. so is args[i]
			    cerr << arg[i] << " ";
			  }
			  cerr << "to be executed\n";
			  args[arg.size()] = NULL;
			  //cerr << prog << " to be executed!\n";
			  execvp(arg[1],args);
			  
			  /*??this part of code has a bug:
			   * if you do exec ./exe more than once,
			   * it won't work from the second time
			   * why?
			   */
			  
			}
			
			
			
			

			cerr << "child process ends\n";
			exit(EXIT_SUCCESS);
		}


		else if(pid < 0){
			cerr << "cannot fork:error\n";
			
			exit(EXIT_FAILURE);
		}

		else{
			cerr << "parent process starts:\n";
			cerr << "child's pid is\t" << pid << endl;
			
			pid = wait(&pstatus);
			if(pid == -1){
				cerr << "wait error\n";
			}

			else{
				if(WIFSIGNALED(pstatus) != 0){
					cerr << "child process ended for signal\t" << WTERMSIG(pstatus)
					      << endl;	
				}			
				else if(WIFEXITED(pstatus) != 0){
					cerr << "child process ended normally\n";
				}

				else{
				cerr << "child process didn't end normally\n";
				}
				
			

		}
		
				



	





	}
}
		







  /*dealing with command that isn't found by the above situations
   * such commands will be sent to the shell and do it the normal way
   * 
   * 
   * 
   * 
   * 

   */
  else {
   
    
    cout << ("normal shell command\n");
    system(cmd);
    
  }
  
    
    
    break;
  }
  //cout << " byebye\n";
  //return 0;
  
}
}
    
    
    
      
    
