# UDP server example
import socket

port = 50000
size = 1024

server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_socket.bind(('', port))

print"UDPServer Waiting for client on port ",port

file = open("transferred","wb")
data,address = server_socket.recvfrom(size)
print "receiving data from ", address[0]


server_socket.sendto("yes",address)


while 1:
	data, address = server_socket.recvfrom(size)
	file.write(data)
	if( data == '\0'):
		break
	##print "client said : ", data
	

print 'transfer done\n'
server_socket.close()

	
