#include "socket.h"


int main(){
  int size = 1024;
  int port = 50000;

  int sock;
  int bytes_read;
  socklen_t addr_len;
  char recv_data[size];
  struct sockaddr_in server_addr, client_addr;
  FILE* file;
  file = fopen("transferred", "wb");

  int timer;
  struct timeval before,after;

  if( (sock = socket(AF_INET,SOCK_DGRAM,0)) == -1){
    fprintf( stderr, "cannot create socket\n");
    return 1;
  }

  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(port);
  server_addr.sin_addr.s_addr = INADDR_ANY;
  bzero(&(server_addr.sin_zero),8);

  if(bind(sock,(struct sockaddr*) &server_addr, sizeof(struct sockaddr)) == -1){
    fprintf(stderr, "cannot bind\n");
    return 2;
  }
  
  addr_len = sizeof(struct sockaddr);
  printf("\nUDP server waiting for client on port %i\n", port);
  fflush(stdout);

  bytes_read = recvfrom(sock,recv_data,size,0, (struct sockaddr*)&client_addr,&addr_len);
  printf ( "receiving data from %s\n", inet_ntoa(client_addr.sin_addr));
  char confirm[] = "yes";
  sendto(sock,confirm,strlen(confirm),0,&client_addr,sizeof(struct sockaddr));
  char ACK[] = "Hey client,I received\n";

  timer = gettimeofday(&before,NULL);
  while (1){
    bzero(recv_data,size);
    bytes_read = recvfrom(sock,recv_data,size,0, (struct sockaddr*)&client_addr,&addr_len);
    if( bytes_read < 0){
      fprintf( stderr, "error in reading from socket\n");
      return 3;
    }
    sendto(sock,ACK,strlen(ACK),0,&client_addr,sizeof(struct sockaddr));
    fwrite(recv_data,1,bytes_read,file);
    if(bytes_read < size)
      break;
    
  }
  timer=gettimeofday(&after,NULL);
  
  
  printf("transfer done\n");
  printf( "it took %f milliseconds to transfer the file\n", (after.tv_sec-before.tv_sec)*1000 + (after.tv_usec-before.tv_usec)/1000.0);
  fclose(file);
  return 0;
}
