# UDP client example
import socket
import sys

port = 50000
size = 1024
host = '130.207.141.38'
client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

if len( sys.argv) != 2:
    print "Usage: client.py <file to send to the server>"
    sys.exit(1)
file=open(sys.argv[1],"rb")


client_socket.sendto("Ready?",(host,port))
data, address = client_socket.recvfrom(size)
if( data == "yes" ):
    print 'server is ready\n'
else:
    print 'please try again\n'
    sys.exit()

i=0
while 1:
    i = i+1
    data = file.read(size)
	##data = raw_input("Type something(q or Q to exit): ")
    
    if (len(data) > 0):
        client_socket.sendto(data, (host,port))
    else:
        client_socket.sendto('\0',(host,port))
        client_socket.close()
        print 'transfer done\n'
        sys.exit()
    print i," : ", len(data), " sent\n"
    data, address = client_socket.recvfrom(size)

