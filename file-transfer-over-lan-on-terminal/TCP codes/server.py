#!/usr/bin/env python

"""
A simple echo server
"""
import socket
import sys


host = ''
port = 50000
backlog = 5
size = 1024
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#opening a tcp socket
s.bind((host,port))#binding the port to socket
s.listen(backlog)
file=open("transferred","wb")


while 1:
    client, address = s.accept()
    while 1:
        data = client.recv(size)
        if( data == '\0' ):
            print 'closing\n'
            client.close()
            sys.exit()
            break
        else:
            ##print "client sent : " , data
            return_text = "Hey client"
            client.send(return_text)
            file.write(data)
    client.close()
    sys.exit(0)
