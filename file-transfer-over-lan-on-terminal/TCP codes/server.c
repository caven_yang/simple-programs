#include "socket.h"
//the c code for the server


int main( int argc, char ** argv){
  int port = 50000;
  int size = 1024;
  int welcome_sock, talk_sock;
  int recv_len;
  char buf[size];
  int backlog = 5;
  struct sockaddr_in server_addr,client_addr;
  socklen_t client_len;
  int true=1;
  FILE* file;
  struct timeval before,after;
  int timer;

  file = fopen("transferred","wb");
  

  welcome_sock = socket(AF_INET,SOCK_STREAM,0);
  if(welcome_sock < 0)
    fprintf(stderr,"error in opening the socket\n" );

  if( setsockopt(welcome_sock,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(int)) == -1){
    fprintf(stderr,"setsockopt error\n");
    exit(1);
  }
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(port);
  server_addr.sin_addr.s_addr = INADDR_ANY;
  bzero(&(server_addr.sin_zero),8);

  if( bind(welcome_sock, (struct sockaddr*) &server_addr, sizeof(server_addr)) < 0)
    fprintf( stderr, "error in binding\n");
  
  listen(welcome_sock, backlog);

  
  while (1){
    client_len = sizeof(client_addr);
    talk_sock = accept( welcome_sock, (struct sockaddr*) &client_addr,&client_len);
    if( talk_sock < 0)
      fprintf(stderr, "error in creating the talking socket\n");
    printf("connected to %s\n", inet_ntoa(client_addr.sin_addr));
    timer = gettimeofday(&before,NULL);

    while(1){
      bzero(buf,size);
      recv_len = read(talk_sock,buf,size);
      if(recv_len < 0)
	fprintf(stderr, "error in reading from socket\n");
 

      else{
	//printf("received message: %i\n", recv_len);
	if( recv_len < size)
	  break;
	fwrite(buf,1,recv_len,file);
	recv_len = write(talk_sock,"Hey client,I received\n", size);	
	//printf( "%i bytes written\n", strlen(buf));
      }
    }
    close(talk_sock);
    timer = gettimeofday(&after,NULL);
    printf( "it took %f milliseconds to transfer the file\n", (after.tv_sec-before.tv_sec)*1000 + (after.tv_usec-before.tv_usec)/1000.0);
    break;
  }
  fclose(file);
  close(welcome_sock);
  return 0;
}
