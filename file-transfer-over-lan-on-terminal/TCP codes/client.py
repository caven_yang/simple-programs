#!/usr/bin/env python

"""
A simple echo client
"""

import socket
import sys
host = '130.207.141.38'
port = 50000
size = 1024
if len(sys.argv)!=2 :
    print "Usage: echoclient.py <file to send to the server>"
    sys.exit(1)
file=open(sys.argv[1],"rb") #read the text
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #tcp socket 
s.connect((host,port))#connect


byte = "ready"

while 1:
    byte=file.read(1024);
    if len(byte) == 0:
        s.send('\0')
        break
    s.send(byte) # blocking call
    ##print 'sent: ', len(byte)
    data = s.recv(size)
    ##print 'Received:', data



s.close()
print 'closing'
sys.exit(0)
