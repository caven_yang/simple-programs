float temperature;
float target = 150, pretarget = 150;
float error[10];
int dtime = 0,predtime = 100;
float delta_t;//dtime and delta_t are for differential part
float pwrate;
float time, pretime;
int interval = 1000, interval_on;
int count = 1;

float read_target(){
  float t = Serial.read();
  return t;
}

float get_t( int pin, int n){//put the pin # and averaging # into the function and return the temperature
  int pinread[2];
  float R=56.11,B=4047,r25 = 50;//resistances in units of kilo_Ohm;
  float Read,sum=0;
  float temper,x;
  for(int i=1;i < n+0.5; i++){
    pinread[0]=analogRead(A0);
    pinread[1]=analogRead(pin);
    Read = (pinread[0]-pinread[1])*R/pinread[1];

    sum = sum + Read;
  }

  
  float thermo = sum/n;

  
  
  

  
  x=1/(273.15 + 25)+log( thermo/r25 )/B;
  temper = 1/x-273.15;
  return temper;
}



void update_error( float tempt, float err[10]){//update tje error array
  for( int i = 9; i >0.3; i--){
    err[i] = err[ i-1]; 
  }
  err[0] = target - tempt;
  
}



float get_power( float err[10]){//PID loop, if differential part is not needed just set delta=1;
  float Pratio,P = 1, I = P/2,scale = 100;
  float D = P*60,diff = 0;
  float integral=0;
  for( int i = 0; i < 9.3; i++){
    integral = integral + err[ i ];
  }
  
  
  diff = ( err[0] - err[1] );//(delta);
  /*
  Serial.println("delta time is");
  Serial.println(delta, DEC);*/
  Pratio = P* err[0] + I* integral + D*diff;
  Serial.println( "P/I/D part is each");
  Serial.println(P*err[0], DEC);
  Serial.println(I*integral, DEC);
  Serial.println(D*diff, DEC);
  Serial.println("Ratio of Power is");
  Serial.println(Pratio, DEC);
  Pratio = Pratio/scale; //tune P and I so that Pratio is a number between 0 and 1
  Pratio = max( 0,Pratio);//to avoid get a bad value for Pratio
  Pratio = min( 1,Pratio);
  if( Pratio > 0.5 ){
    Pratio = 0.999;
    }
  Serial.println("After amendment");
  Serial.println(Pratio, DEC);
  return Pratio;
}



void turn_on( int pin){ //turn the heat with #pin on
  digitalWrite( pin, HIGH);
}

void turn_off( int pin){//turn the heat with #pin off
  digitalWrite( pin, LOW);
}




void setup(){
  Serial.begin(9600);
  pinMode( A0, INPUT);
  pinMode( A1, INPUT);
  pinMode( 1, OUTPUT);
  pinMode( 3, OUTPUT);
  pinMode( 5, OUTPUT);
//initialize the error array

for( int i=0; i<9.3; i++){
  temperature = get_t( A1, 30);
  //update_error( temperature, error);
  //Serial.println(error[i], DEC);
  error[i] = target - temperature;}
  pwrate = get_power( error);// 100.0);  
  interval_on = interval * pwrate;    
  time = pretime = millis();
  
}

void loop() {
  
  
  
  if (Serial.read() < 10 ){
    Serial.println("No signal from PC");
    target = pretarget;
  }
  
  else{
    pretarget = target;
    target = Serial.read();
  }
  Serial.println("current temperature is");
  Serial.println(temperature, DEC);
  /*predtime = dtime;
  dtime = millis();
  float delta_t = (dtime - predtime)/1000.0;
  Serial.println("in loop dtime is");
  Serial.println(delta_t, DEC);*/
  
  
  
  if( ( millis() - pretime) < interval_on){
    turn_on(5);
    Serial.println("# of ON-state is");
    Serial.println(count, DEC);
    count++;}

    
  else if( ((millis() - pretime) > interval_on) && (millis() - pretime) < interval){
    turn_off(5);}   
    
    
  else if( (millis() - pretime) > interval){
    turn_off(5);        
    Serial.println("Recalculating # of ON-state");
    Serial.println("Target temperature is");
    Serial.println(target, DEC);
    temperature = get_t(A1, 30);
    update_error( temperature, error);
    pwrate = get_power( error);//, delta_t);
        
    interval_on = interval * pwrate;        
    //Serial.println(interval_on, DEC);
    pretime = millis();
    count = 1;}
    
    
    
    
    
    
    
    

Serial.println("1 is on & 0 is off");
Serial.println(digitalRead(5), DEC);
}







