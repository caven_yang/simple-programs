float temperature[4];
float target[4] = {130, 140, 150, 160};
float pretarget[4] = {80, 140, 150, 160};
float error[4][10];
int dtime[4] = {0,0,0,0},predtime[5] = {100, 100, 100, 100};
float delta_t[4];//dtime and delta_t are for differential part
float pwrate;
float time[4], pretime[4];
int interval = 1000, interval_on[4];
int count[4] = {1,1,1,1};
int readpin[4] = {A1, A2, A3, A4};
int out[4] = {5, 6, 8, 9};



float read_target(){
  float t = Serial.read();
  return t;
}

float get_t( int pin, int n){//put the pin # and averaging # into the function and return the temperature
  int pinread[2];
  float R=56.11,B=4047,r25 = 50;//resistances in units of kilo_Ohm;
  float Read,sum=0;
  float temper,x;
  for(int i=1;i < n+0.5; i++){
    pinread[0]=analogRead(A0);
    pinread[1]=analogRead(pin);
    Read = (pinread[0]-pinread[1])*R/pinread[1];

    sum = sum + Read;
  }

  
  float thermo = sum/n;

  
  
  

  
  x=1/(273.15 + 25)+log( thermo/r25 )/B;
  temper = 1/x-273.15;
  return temper;
}



void update_error( float tempt, float err[10],int j){//update the error array
  for( int i = 9; i >0.3; i--){
    err[i] = err[ i-1]; 
  }
  err[0] = target[j] - tempt;
  
}



float get_power( float err[10]){//PID loop, if differential part is not needed just set delta=1;
  float Pratio,P = 1, I = P/2,scale = 800;
  float D = P*60,diff = 0;
  float integral=0;
  for( int i = 0; i < 9.3; i++){
    integral = integral + err[ i ];
  }
  
  
  diff = ( err[0] - err[1] );//(delta);
  /*
  Serial.println("delta time is");
  Serial.println(delta, DEC);*/
  Pratio = P* err[0] + I* integral + D*diff;
  Serial.println( "P/I/D part is each");
  Serial.println(P*err[0], DEC);
  Serial.println(I*integral, DEC);
  Serial.println(D*diff, DEC);
  Serial.println("Ratio of Power is");
  Serial.println(Pratio, DEC);
  Pratio = Pratio/scale; //tune P and I so that Pratio is a number between 0 and 1
  Pratio = max( 0,Pratio);//to avoid get a bad value for Pratio
  Pratio = min( 1,Pratio);
  if( Pratio > 0.5 ){
    Pratio = 0.999;
    }
  Serial.println("After amendment");
  Serial.println(Pratio, DEC);
  return Pratio;
}



void turn_on( int pin){ //turn the heat with #pin on
  digitalWrite( pin, HIGH);
}

void turn_off( int pin){//turn the heat with #pin off
  digitalWrite( pin, LOW);
}




void setup(){
  Serial.begin(9600);
  for (int i=0; i<4.5; i++){
    pinMode( readpin[i], INPUT);
    pinMode( out[i], OUTPUT);//initialize the error array
    
      for(int j=0; j<9.5; j++){
        temperature[i] = get_t( readpin[i],30);
        error[i][j] = target[i] - temperature[i];}
        
        pwrate= get_power( error[i]);
        interval_on[i] = interval * pwrate;
        time[i] = pretime[i] = millis();
        
        
        
  
}
  


}
  
  
 
 
void loop(){
  for (int i = 0; i < 3.5; i++){
    Serial.print("Working on Control No.");
    Serial.println(i+1, DEC);
    Serial.println("current time is,");
    Serial.println(millis()/1000.00, DEC);
    Serial.println("pretime[i] is,");
    Serial.println(pretime[i]/1000.00, DEC);
    Serial.println("current temperature is");
    Serial.println(temperature[i], DEC);
    if (Serial.read() < 10 ){
    Serial.println("No signal from PC");
    target[i] = pretarget[i];
    }
  
    else{
      pretarget[i] = target[i];
      target[i] = Serial.read();
    }

    if( ( millis() - pretime[i]) < interval_on[i]){
      turn_on(out[i]);
    Serial.println("# of ON-state is");
    Serial.println(count[i], DEC);
    count[i]++;}

    
  else if( ((millis() - pretime[i]) > interval_on[i]) && (millis() - pretime[i]) < interval){
    turn_off(out[i]);
    Serial.println("# of off state");
    Serial.println( count[i],DEC);}   
    
    
  else if( (millis() - pretime[i]) > interval){
    turn_off(out[i]);        
    Serial.println("Recalculating # of ON-state");
    Serial.println("Target temperature is");
    Serial.println(target[i], DEC);
    temperature[i] = get_t(readpin[i], 30);
    update_error( temperature[i], error[i],i);
    pwrate = get_power( error[i]);//, delta_t);
        
    interval_on[i] = interval * pwrate;        
    //Serial.println(interval_on[i], DEC);
    pretime[i] = millis();
    count[i] = 1;}
    
    
    
  
  
  
  
  
}
}
