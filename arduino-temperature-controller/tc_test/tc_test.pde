float get_tc( int pin, int n){
  float sum = 0;
  float temper;
  
  
  for( int i =1; i < n+0.3;i++){
    temper = analogRead(pin);
    sum = sum + temper;
  }
  temper = sum/n;
  return temper/2.0;
  
}


void setup(){
  Serial.begin(9600);
  pinMode(A5, INPUT);
}

void loop(){
  Serial.println( get_tc( A5,40));
}
    
  
