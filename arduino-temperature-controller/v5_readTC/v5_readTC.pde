float temperature[4];
float target[4] = {160,160,160,160};
float pretarget[4] ={160,160,160,160};
float errorsum[4];
float error[4];
int dtime[4] = {0,0,0,0},predtime[5] = {100, 100, 100, 100};
float delta_t;//dtime and delta_t are for differential part
float pwrate;
float time[4], pretime[4];
int interval = 3000, interval_on[4];
int count[4] = {1,1,1,1};
int readpin[4] = {A1, A1, A4, A4};
int out[4] = {8, 6, 5, 9};
int counter = 1;
float target1 = 10;
int led[4]={1,2,3,4};

float reset(int choose){
  float temp1, temp2;
  temp1 = min( Serial.read(), Serial.read());
  temp1 = min( temp1, Serial.read());
  temp1 = min( temp1, Serial.read());
  temp2 = max( Serial.read(), Serial.read());
  temp2 = max( temp2, Serial.read());
  temp2 = max( temp2, Serial.read());
  if ( choose < 5){
    return temp1;}
  else{
    return temp2;}
}
      


float read_target(){
  float t = Serial.read();
  return t;
}



float get_tc( int pin, int n){
  float sum = 0;
  float temper;
  
  
  for( int i =1; i < n+0.3;i++){
    temper = analogRead(pin);
    sum = sum + temper;
  }
  temper = sum/n;
  return temper/2.0;
  
}


float get_t( int pin, int n){//put the pin # and averaging # into the function and return the temperature
  int pinread[2];
  float R=56.11,B=4047,r25 = 50;//resistances in units of kilo_Ohm;
  float Read,sum=0;
  float temper,x;
  for(int i=1;i < n+0.5; i++){
    pinread[0]=analogRead(A0);
    pinread[1]=analogRead(pin);
    Read = (pinread[0]-pinread[1])*R/pinread[1];

    sum = sum + Read;
  }

  
  float thermo = sum/n;

  
  
  

  
  x=1/(273.15 + 25)+log( thermo/r25 )/B;
  temper = 1/x-273.15;
  return temper;
}


float sumerr(float err, float errsum){
  float ratiosum = 40, ratioerr = 60,errsum1;
  errsum1 = (errsum*ratiosum + err*ratioerr)/(ratiosum+ratioerr);
  
return errsum1;


}
 

float getpower(float errsum,float tar){//calculating the power using averaging, return a value from 0 to 1

  float pratio;
  pratio = errsum/15.0 + tar/280.0;
  //Serial.println("raw power rate is");
  //Serial.println(pratio, DEC);
  
  
  pratio = max(pratio, 0);
  pratio = min(1, pratio);
  if( pratio > 0.6){
    pratio = 0.99;}
  return pratio;
  
}




void turn_on( int pin){ //turn the heat with #pin on
  digitalWrite( pin, HIGH);
}

void turn_off( int pin){//turn the heat with #pin off
  digitalWrite( pin, LOW);
}


void setup(){
  Serial.begin(9600);
  pinMode(A5, INPUT);
  for(int i=0;i<3.5;i++){
    pinMode( readpin[i], INPUT);
    pinMode( out[i], OUTPUT);//initialize the error array
    pinMode( led[i],OUTPUT);
    temperature[i]= get_t( readpin[i],30);
    error[i] = target[i] - temperature[i];
    errorsum[i] = error[i];
    pwrate= getpower(errorsum[i],target[i]);
    interval_on[i] = interval * pwrate;
    time[i] = pretime[i] = millis();
    
    
  }
  
  for (int j=0; j<1.2;j++){
    float t=25.0;
    Serial.println(t, DEC);
}

}


void loop(){
  target1 = reset(8);
  int counter = reset(1);
  //Serial.print("target 1 and counter:   ");
  //Serial.println(target1, DEC);
  //Serial.println(counter, DEC);
  
  
  
  
  for( int i=0; i<3.4; i++){

    
    if( (millis() - pretime[i]> interval)){
      turn_off(out[i]);
      //Serial.println("Recalculating # of ON-state");
     
        
      temperature[i] = get_t(readpin[i], 30);
      //if(temperature[i]<10){
        //temperature[i]=target[i]*2;
        //Serial.println("error: thermistor is broken");
      //}
     
      error[i] = target[i]- temperature[i];
      errorsum[i] = sumerr(error[i], errorsum[i]);

      pwrate = getpower(errorsum[i],target[i]);
      interval_on[i]=pwrate*interval;   
      //Serial.println("KK");
      //Serial.print("Working on Control No.");
      Serial.println(i+1, DEC);
      //Serial.print("interval_on is");
      //Serial.println(interval_on[i], DEC);  
      pretime[i] = millis();
      count[i] = 1;
      //Serial.print("current temperature is  ");
      if(temperature[i] < 20){
        turn_off(led[i]);}
      else{
        turn_on(led[i]);}
      Serial.println(temperature[i], DEC);
      //Serial.print("Target temperature is  ");
      Serial.println(target[i], DEC); 
      Serial.println(get_tc(A5,40),DEC);
      //Serial.print("sum of error: ");
      //Serial.println(errorsum[i],DEC);
      //Serial.print("Thermal couple reads: ");
      
      
    }
      
      
    else if( ((millis() - pretime[i])>interval_on[i]) && ((millis() - pretime[i]) < interval)){
      turn_off(out[i]);
      //Serial.print("# of off:");
      //Serial.println(count[i], DEC);
      //Serial.print("f");
      count[i]++;}
      
      
      
      
    else if ((millis() - pretime[i])< interval_on[i]){
      turn_on(out[i]);
      //Serial.print("N");
      //Serial.print("# of on:");
      //Serial.println(count[i], DEC);
      count[i]++;}
  
  
  
  

  
  
  


  
  
}

}


